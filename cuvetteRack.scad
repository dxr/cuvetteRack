size=4.5;               // Cuvette second dimension
depth=25;              // Hole depth
Offset=0.6;             // Additional space
wallThickness=1;
rowSpaceing=5.9;
columnSpaceing=1;

row=3;                 // Number of rows
column=5;             // Number of columns

base=0;                // 1 if base on, 0 if off
baseX=50;
baseY=100;

module cuvette(size) {
    translate([0, 0, 25/2]) union() {
        cube([size + Offset, 12.5 + Offset, depth], center=true);
        cube([size + Offset + 2, 12.5 + Offset - 2, depth], center=true);
        cube([size + Offset - 2, 12.5 + Offset + 2, depth], center=true);
    }
}

cubeX = (size + Offset + 2)*row + rowSpaceing*(row-1) + 2*wallThickness;
cubeY = (12.5 + Offset + 2)*column + columnSpaceing*(column-1) + 2*wallThickness;

trX = (size + Offset + 2)/2 + wallThickness;
trY = (12.5 + Offset + 2)/2 + wallThickness;

difference() {
    union() {
        cube([cubeX, cubeY, depth]);
        if(base==1) translate([(cubeX-baseX)/2, (cubeY-baseY)/2, 0]) cube([baseX, baseY, 1.5]);
    }
    for(i = [0 : 1 : row-1]) {
        for(j = [0 : 1 : column-1]) {
            translate([trX + i*(size + Offset + 2 + rowSpaceing), trY + j*(12.5 + Offset + 2 + columnSpaceing), 1]) cuvette(size);
            translate([trX + i*(size + Offset + 2 + rowSpaceing), trY + j*(12.5 + Offset + 2 + columnSpaceing), 1]) cube([3, 5, 5], center=true);
        }
    }
}